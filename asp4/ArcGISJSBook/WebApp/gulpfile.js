﻿var gulp = require('gulp');
var jshint = require('gulp-jshint');
//var stylish = require('jshint-stylish');

gulp.task('default',['lint'], function () {
    console.log('completed...');
});

gulp.task('lint', function () {
    return gulp.src('app/js/app.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
});