define([
    //class
    "dojo/_base/declare",
    "dojo/_base/lang",

    //widgit class
    "dijit/_WidgetBase",

    //templated widgit
    "dijit/_TemplatedMixin",
    "dojo/i18n!widgets/GoogleGeocodeWidget/nls/strings",
    //loading template file
    "dojo/text!widgets/GoogleGeocodeWidget/template/_geocode.html",

    "dijit/a11yclick",
    "dojo/dom-style",
    "dojo/on",
    "dojo/request",
    'esri/geometry/Point', 'esri/SpatialReference',
    "dojo/domReady!"
], function (
    declare, lang,
    _WidgetBase,
    _TemplatedMixin,
    nls,
    dijitTemplate,
    a11yclick,
    domStyle,
    on,
    request,
    Point, SpatialReference) {
    return declare([_WidgetBase, _TemplatedMixin], {
        //assigning html template to template string
        templateString: dijitTemplate,

        options: {
            mapObject: null
        },
        constructor: function (options, srcRefNode) {
            // widget node
            this.domNode = srcRefNode;
            // localized strings
            this.nls = nls;
            var defaults = lang.mixin({}, this.options, options);
            // properties
            this.set("map", defaults.map);

        },

        postCreate: function () {
            this.inherited(arguments);
            // events
            this.own(
                // setup an event handler (automatically remove() when destroyed)
                on(this.search, a11yclick, lang.hitch(this, this.searchAddress))
            );
        },
        startup: function () {

        },
        searchAddress: function () {
            var address = this.address_input.value;

            var requestURL = this.nls.apiUrl + "?address=" + address + "&key=" + this.nls.apiKey;
            request(requestURL, {
                headers: {
                    "X-Requested-With": null
                },
                handleAs: "json"
            }).then(lang.hitch(this, function (data) {
                // do something with handled data
                console.log(data);
                console.log(this.map);
                this.zoomTo(data.results[0].geometry.location.lng,data.results[0].geometry.location.lat);
            }), function (err) {
                // handle an error condition
                console.log(err);
            });
        },
        zoomTo: function (x, y) {
            console.log(x + ',' + y);
            var point = new Point([x, y], new SpatialReference({
                wkid: 4326
            }));
            this.map.centerAndZoom(point, 7);
        },
    });
});