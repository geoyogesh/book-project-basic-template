define([
    //class
    "dojo/_base/declare",
    "dojo/_base/lang",

    //widgit class
    "dijit/_WidgetBase",

    //templated widgit
    "dijit/_TemplatedMixin",

    //loading template file
    "dojo/text!widgets/SpatialQuery/template/_spatialquery.html",

    "dojo/on",
	"dijit/a11yclick",
    "dojo/dom-style",
    "esri/toolbars/draw",
    "esri/tasks/query",
	"esri/tasks/QueryTask",
	"esri/layers/FeatureLayer",
"esri/InfoTemplate",
    "dojo/dom-class",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
	"esri/symbols/PictureMarkerSymbol",
    "esri/graphic",
    "dojo/_base/Color",

    "dojo/domReady!"
], function (
	declare,
	lang,
	_WidgetBase,
	_TemplatedMixin,
	dijitTemplate,
	on,
	a11yclick,
	domStyle,
	Draw,
	Query,
	QueryTask,
	FeatureLayer,
	InfoTemplate,
	domClass,
	SimpleFillSymbol,
	SimpleLineSymbol,
	PictureMarkerSymbol,
	Graphic, Color) {
	return declare("hazardeventwidget", [_WidgetBase, _TemplatedMixin], {
		//assigning html template to template string
		templateString: dijitTemplate,
		isDrawActive: false,
		map: null,
		tbDraw: null,
		earthQuakeLayerURL: 'http://maps.ngdc.noaa.gov/arcgis/rest/services/web_mercator/hazards/MapServer/5',
		constructor: function (options, srcRefNode) {
			this.map = options.map;
		},
		startup: function () {},
		postCreate: function () {
			this.inherited(arguments);
			// events
			this.own(
				/* setup an event handler (automatically remove() when destroyed) */
				on(this.btndrawpoly, 'click', lang.hitch(this, this.toggleDraw))
			);
			this.tbDraw = new Draw(this.map);
			this.tbDraw.on("draw-end", lang.hitch(this, this.querybyGeometry));
		},
		toggleDraw: function () {
			domClass.toggle(this.btndrawpoly, "btn-danger");
			if (!this.isDrawActive) {
				this.tbDraw.activate(Draw.POLYGON);
				this.isDrawActive = true;
			} else {
				this.tbDraw.deactivate();
				this.isDrawActive = false;
			}
		},
		querybyGeometry: function (evt) {
			this.tbDraw.deactivate();
				this.isDrawActive = false;
			this.isBusy(true);

			var geometryInput = evt.geometry;
			var tbDrawSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT, new Color([255, 255, 0]), 2), new Color([255, 255, 0, 0.2]));

			this.map.graphics.clear();
			var graphicPolygon = new Graphic(geometryInput, tbDrawSymbol);

			this.map.graphics.add(graphicPolygon);
			//Getting the data
			var queryTask = new QueryTask(this.earthQuakeLayerURL);
			var query = new Query();
			query.where = "EQ_MAGNITUDE > 6";
			query.geometry = geometryInput;
			query.returnGeometry = true;
			query.outFields = ["LOCATION_NAME", "EQ_MAGNITUDE", "YEAR", "MONTH"];
			queryTask.execute(query).then(lang.hitch(this,
					function (result) {
						this.map.graphics.clear();
						/*Success Handler*/
						var symbolSelected = new PictureMarkerSymbol({
							"angle": 0,
							"xoffset": 0,
							"yoffset": 0,
							"type": "esriPMS",
							"url": "http://static.arcgis.com/images/Symbols/Basic/esriCrimeMarker_86.png",
							"contentType": "image/png",
							//"width": 24,
							//"height": 24
						});
						var str = '';
						for (var i = 0; i < result.features.length; i++) {
							var featAttr = result.features[i].attributes;
							var featGeom = result.features[i].geometry;
							var magnitude = Math.ceil(featAttr.EQ_MAGNITUDE * 10) / 10;
							symbolSelected.width = symbolSelected.height = Math.ceil(magnitude * 4);
							var infoTemplate = new InfoTemplate(featAttr.LOCATION_NAME, "Magnitude:" + magnitude + "<br/>YEAR:" + featAttr.YEAR);
							var selectionGraphic = new Graphic(featGeom, symbolSelected, null, infoTemplate);
							this.map.graphics.add(selectionGraphic);
							str = str + '<tr><th scope="row">' + (i + 1) + '</th><td>' + featAttr.LOCATION_NAME + '</td><td>' + magnitude + '</td><td>' + featAttr.YEAR + '</td></tr>';
						}
						this.map.infoWindow.show();
						this.tbcontent.innerHTML = str;
						this.isBusy(false);
					}),
				function (err) {
					/*Error handler*/
					console.log(err);
					this.isBusy(false);
				});
		},
		isBusy: function (isBusy) {
			if (isBusy) {
				domStyle.set(this.loadingdiv, 'display', 'block');
			} else {
				domStyle.set(this.loadingdiv, 'display', 'none');
			}

		}
	});
});