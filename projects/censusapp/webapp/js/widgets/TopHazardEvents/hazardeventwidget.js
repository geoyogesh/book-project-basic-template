define([
    //class
    "dojo/_base/declare",
    "dojo/_base/lang",

    //widgit class
    "dijit/_WidgetBase",

    //templated widgit
    "dijit/_TemplatedMixin",
    "dojo/i18n!widgets/TopHazardEvents/nls/strings",
    //loading template file
    "dojo/text!widgets/TopHazardEvents/template/_events.html",
"dojo/string",
    'dgrid/Grid',
    'dgrid/Keyboard',
    'dgrid/Selection',

    "dojo/dom-style",
    "esri/tasks/query", "esri/tasks/QueryTask",
    "dojo/domReady!"
], function (
	declare, lang,
	_WidgetBase,
	_TemplatedMixin,
	nls,
	dijitTemplate,
	string,
	Grid, Keyboard, Selection,

	domStyle,
	Query, QueryTask) {
	return declare("hazardeventwidget", [_WidgetBase, _TemplatedMixin], {
		//assigning html template to template string
		templateString: dijitTemplate,
		grid: null,
		constructor: function (options, srcRefNode) {
			this.nls = nls;
			this.map = options.map;
		},
		startup: function () {
			this.isBusy(true);
			this._nls = nls;
			//getting the data
			var query = new Query();
			var queryTask = new QueryTask('http://maps.ngdc.noaa.gov/arcgis/rest/services/web_mercator/hazards/MapServer/6');
			query.where = "DEATHS_AMOUNT_ORDER > 3";
			query.returnGeometry = true;
			query.outFields = ["*"];
			query.outSpatialReference = this.map.spatialReference;
			queryTask.execute(query).then(lang.hitch(this, function (result) {
				console.log(result);
				var data = [];

				for (var i = 0; i < result.features.length; i++) {
					data.push({
						'id': i + 1,
						'name': result.features[i].attributes.NAME,
						'location': result.features[i].attributes.LOCATION,
						'geometry': result.features[i].geometry
					});
				}
				var CustomGrid = declare([Grid, Keyboard, Selection]);
				var grid = new CustomGrid({
					columns: {
						id: 'id',
						name: 'name',
						location: 'location'
					},
					selectionMode: 'single',
					cellNavigation: false
				}, this.eventsgrid);
				grid.renderArray(data);

				grid.on('dgrid-select', lang.hitch(this, function (event) {
					this.map.centerAndZoom(event.rows[0].data.geometry);
					var locQuery = new Query();
					locQuery.where = "LOCATION = '" + event.rows[0].data.location + "'";
					queryTask.executeForCount(locQuery, lang.hitch(this, function (evt) {
						this.CountMessage.innerHTML = string.substitute("There are ${0} volcanoes at ${1}", {
							0: evt,
							1: event.rows[0].data.location
						});
					}));
					queryTask.executeForExtent(locQuery, lang.hitch(this, function (evt) {
						this.map.setExtent(evt.extent);
					}));
				}));

				this.isBusy(false);
			}), function (err) {
				console.log(err);
				this.isBusy(false);
			});

		},
		isBusy: function (isBusy) {
			if (isBusy) {
				domStyle.set(this.loadingdiv, 'display', 'block');
			} else {
				domStyle.set(this.loadingdiv, 'display', 'none');
			}

		}
	});
});