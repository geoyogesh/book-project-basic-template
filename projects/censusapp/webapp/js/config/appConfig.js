define([],function(){
	return{
		geometryServiceURL : "http://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer",
		earthQuakeLayerURL : 'http://maps.ngdc.noaa.gov/arcgis/rest/services/web_mercator/hazards/MapServer/5',
		shadedTileLayerURL : 'http://maps.ngdc.noaa.gov/arcgis/rest/services/web_mercator/etopo1_hillshade/MapServer',
		worldCitiesLayerURL : "http://maps.ngdc.noaa.gov/arcgis/rest/services/SampleWorldCities/MapServer"
	}
})