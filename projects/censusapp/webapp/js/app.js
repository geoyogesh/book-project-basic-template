var map = null;
require(["esri/map",
         "esri/dijit/BasemapGallery",
         "esri/dijit/Legend",
         "dojo/dom",
         "dojo/dom-class",
         "dojo/on",
         "esri/toolbars/draw",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/SimpleLineSymbol",
              "esri/graphic",
        "dojo/_base/Color",
         "esri/layers/ArcGISTiledMapServiceLayer",
         "esri/layers/ArcGISDynamicMapServiceLayer",
         "esri/layers/FeatureLayer",
         "esri/config",
         "esri/urlUtils",
         "dojo/_base/array",
         "dojo/parser",
		 "config/appConfig",

         "esri/dijit/Basemap",
         "widgets/SpatialQuery/spatialquery",
         "widgets/TopHazardEvents/hazardeventwidget",
         "widgets/GoogleGeocodeWidget/google_geocode",
         "esri/InfoTemplate",
         "esri/dijit/InfoWindow",
		 "esri/dijit/PopupTemplate",
		 "esri/dijit/Popup",
         "dojo/domReady!"],
	function (Map, BasemapGallery, Legend, dom, domClass, on, Draw, SimpleFillSymbol, SimpleLineSymbol, Graphic, Color, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, FeatureLayer, esriConfig, urlUtils, arrayUtils, parser, config,
		Basemap,
		Spatialquery,
		Attrquery,
		GoogleGeocode,
		InfoTemplate,
		InfoWindow,
		PopupTemplate,
		Popup) {
		parser.parse();
		var earthQuakeLayerURL = config.earthQuakeLayerURL;
		var shadedTileLayerURL = config.shadedTileLayerURL;
		var worldCitiesLayerURL = config.worldCitiesLayerURL;

		var map = new Map("mapDiv");

		on(map, "layers-add-result", function (evt) {
			var legendDijit = new Legend({
				map: map
			}, "legendDiv");
			legendDijit.startup();
		});
		var shadedTiledLayer = new ArcGISTiledMapServiceLayer(shadedTileLayerURL);

		var worldCities = new ArcGISDynamicMapServiceLayer(worldCitiesLayerURL, {
			id: "worldCities",
			"opacity": 0.5,
			"showAttribution": false
		});
		worldCities.setVisibleLayers([0]);
		worldCities.setLayerDefinitions(["POP > 1000000"]);

		var genericTemplate = new InfoTemplate("Attributes", "${*}");
		var popupTemplate = new PopupTemplate({
			title: "{LOCATION_NAME}",
			description: "An earthquake of magnitude <b> {EQ_MAGNITUDE} </b> Ritchter Scale and intensity <b>{INTENSITY}</b> hit this place on {DATE_STRING}",
			fieldInfos: [
				{
					fieldName: "LOCATION_NAME",
					label: "LOCATION",
					visible: true
				},
				{
					fieldName: "EQ_MAGNITUDE",
					label: "Magnitude",
					visible: true
				},
				{
					fieldName: "INTENSITY",
					label: "Intensity",
					visible: true
				},
				{
					fieldName: "DAMAGE_DESCRIPTION",
					label: "Damage",
					visible: true
				},
				{
					fieldName: "DATE_STRING",
					label: "Date",
					visible: true,
					format: {
						dateFormat: 'longMonthDayYear'
					}
				}
	],
			mediaInfos: [{
					type: "barchart",
					value: {
						fields: ["EQ_MAGNITUDE", "INTENSITY"],
						theme: "Julie",
						tooltipField : "EQ_MAGNITUDE"					}
				}

					]


		});
		var earthQuakeLayer = new FeatureLayer(earthQuakeLayerURL, {
			id: "Earthquake Layer",
			outFields: ["EQ_MAGNITUDE", "INTENSITY", "COUNTRY", "LOCATION_NAME", "DAMAGE_DESCRIPTION", "DATE_STRING"],
			opacity: 0.5,
			mode: FeatureLayer.MODE_ONDEMAND,
			definitionExpression: "EQ_MAGNITUDE > 6",
			infoTemplate: popupTemplate

		});


		map.addLayers([shadedTiledLayer, worldCities, earthQuakeLayer]);
		/*
		on(earthQuakeLayer, "mouse-over", function (evt) {
			map.infoWindow.setTitle('earthq');
			map.infoWindow.setContent('hello<br/>world');
			map.infoWindow.show(evt.screenPoint, InfoWindow.ANCHOR_UPPERRIGHT);
			map.setMapCursor("pointer");
		});
		on(earthQuakeLayer, "mouse-out", function (evt) {
			map.infoWindow.hide();
			map.setMapCursor("default");
		});
		*/


		on(map, "layers-add-result", function (evt) {
			console.log("1.", earthQuakeLayer.id);
			console.log("2.", earthQuakeLayer.fields);
			console.log("3.", earthQuakeLayer.geometryType);
			console.log("4.", earthQuakeLayer.maxRecordCount);

			console.log("5.", worldCities.layerInfos);
		});



		var basemapGallery = new BasemapGallery({
			showArcGISBasemaps: true,
			map: map
		}, "basemapGalleryDiv");

		var basemap = new Basemap({
			layers: [shadedTiledLayer],
			title: "HillShade",
			thumbnailUrl: "images/releif.png"
		});
		basemapGallery.add(basemap);

		basemapGallery.startup();

		var ggeocodewidget = new GoogleGeocode({
			map: map
		}, "ggeocodewidget");
		ggeocodewidget.startup();

		on(map, "load", function (evt) {
			var spatialquery = new Spatialquery({
				map: map
			}, "spatialqueryspan");
			spatialquery.startup();

			var attrquery = new Attrquery({
				map: map
			}, "attrqueryspan");
			attrquery.startup();
		});

	});