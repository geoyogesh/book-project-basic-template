define([
    //class
    "dojo/_base/declare",
 	"dijit/_TemplatedMixin",
	"dojo/on",
	"dojo/_base/lang",
    //Plugin to load HTML Template file
    "dojo/text!widgets/SLD/template/_widget.html",

    //widgit class
    "dijit/_WidgetBase",
	"dojox/charting/Chart",
	"dojox/charting/axis2d/Default",
	"dojox/charting/plot2d/Bars",
	"dojox/charting/action2d/TouchZoomAndPan",
	"dojox/charting/action2d/TouchIndicator",
	"dojox/charting/themes/Dollar",
    "dojo/domReady!"
], function (
	declare,
	_TemplatedMixin,
	on,
	lang,
	dijitTemplate,
	_WidgetBase,
	Chart, Default, Bars, MouseZoomAndPan, MouseIndicator, Dollar
) {
	return declare([_WidgetBase, _TemplatedMixin], {
		templateString: dijitTemplate,
		c: null,
		x: 1,
		constructor: function (options) {


		},

		postCreate: function () {
			this.inherited(arguments);

		},

		startup: function () {
			console.log(this.templateString);
			this.createChart();
		},

		destroy: function () {},

		createChart: function () {




			this.c = new Chart(this.chartDiv);
			this.c.addPlot("default", {
					type: Bars,
					gap: 2
				}).addAxis("x", {
					natual: true,
					includeZero: true,
					majorTickStep: 1
				})
				.addAxis("y", {
					natual: true,
					vertical: true
				}).
			addSeries("A", [1.2, 3.1, 5.5, 7.5, 2.5, 4.6, 6.6]).
			/*addSeries("B", [2, 4, 6, 8, 3, 5, 7], {
				hide: true
			}).
			addSeries("C", [6, 4, 2, 7, 5, 3, 1]).*/
				setTheme(Dollar);
			MouseZoomAndPan(this.c, "default", {
				axis: "x",
				enableScroll: false,
				enableZoom: false
			});
			MouseIndicator(this.c, "default", {
				series: "A",
				dualIndicator: true,
				fixed : false,
				precision : 2,
				font: "normal normal bold 16pt Tahoma"				
			});
			this.c.render();

			var that = this;

			on(this.plus, 'click', lang.hitch(function (e) {
				that.c.setWindow(that.x++, 1, 250, 0).render();
			}));
			on(this.minus, 'click', lang.hitch(function (e) {
				that.c.setWindow(1, 1, 0, 0).render();
			}));
		}
	});
});