var gulp = require('gulp');
var download = require("gulp-download");
var unzip = require("gulp-unzip");
var del = require('del');
var shell = require('gulp-shell');
var gulpIgnore = require('gulp-ignore');
var rimraf = require('gulp-rimraf');
var gulpif = require('gulp-if');

gulp.task('setup',['clean:apiCache'], function() {
  var basePath = 'https://github.com/Esri/enterprise-build-sample-js/raw/master/apiCache/';
  return download([basePath +'js-dgrid-0.3.17.zip',
            basePath + 'js-dijit-1.10.4.zip',
            basePath+'js-dojo-1.10.4.zip',
            basePath+'js-dojox-1.10.4.zip',
            basePath+'js-esri-3.13-amd.zip',
            basePath+'js-put-selector-0.3.6.zip',
            basePath+'js-util-1.10.4.zip',
            basePath+'js-xstyle-0.1.3.zip'])
    .pipe(unzip())
    .pipe(gulp.dest("apiCache/"));
});


gulp.task('copy:publish', function() {
  return gulp.src(['webapp/**/*'])
          .pipe(gulp.dest("publish/"));
});
gulp.task('copy-lib:publish', function() {
  var libFolder ='apiCache';
  var publishFolder ='publish';
   gulp.src([libFolder+'/js-dgrid-0.3.17/**/*']).pipe(gulp.dest(publishFolder+"/js/dgrid/"));
   gulp.src([libFolder+'/js-dijit-1.10.4/**/*']).pipe(gulp.dest(publishFolder+"/js/dijit/"));
   gulp.src([libFolder+'/js-dojo-1.10.4/**/*']).pipe(gulp.dest(publishFolder+"/js/dojo/"));
   gulp.src([libFolder+'/js-dojox-1.10.4/**/*']).pipe(gulp.dest(publishFolder+"/js/dojox/"));
   gulp.src([libFolder+'/js-esri-3.13-amd/esri/**/*']).pipe(gulp.dest(publishFolder+"/js/esri/"));
   gulp.src([libFolder+'/js-put-selector-0.3.6/**/*']).pipe(gulp.dest(publishFolder+"/js/put-selector/"));
   gulp.src([libFolder+'/js-util-1.10.4/**/*']).pipe(gulp.dest(publishFolder+"/js/util/"));
   gulp.src([libFolder+'/js-xstyle-0.1.3/**/*']).pipe(gulp.dest(publishFolder+"/js/xstyle/"));
   gulp.src(['profiles/publish.profile.js']).pipe(gulp.dest(publishFolder+"/js/util/buildScripts/profiles/"));
});

gulp.task('clean:publish', function() {
  return del(['publish/**/*'])
});
gulp.task('clean:apiCache', function() {
  return del(['apiCache/**/*'])
});

gulp.task('default',['clean:publish','copy:publish','copy-lib:publish','build:run','build:post1'], function() {
  console.log('completed...');
});

gulp.task('build:run', shell.task([
  'publish\\js\\util\\buildscripts\\build.bat -p publish action=release optimize=shrinksafe copytests=false internStrings=true releaseName=buildProjectRelease cssOptimize=comments'
]));

gulp.task('build:post1', function() {
  var publishFolder ='publish';
 gulp.src([publishFolder+'/js/release/buildProjectRelease/dojo/dojo.js']).pipe(gulp.dest(publishFolder+'/js/dojo/'));
  gulp.src([publishFolder+'/js/release/buildProjectRelease/dojo/nls/dojo.js']).pipe(gulp.dest(publishFolder+'/js/dojo/nls/'));
});



var condition = function (file) {
    console.log(file['cwd']);
    if (file['cwd'].indexOf('images')>=0)   
    {
        
        return true;
    }
  return false;
}


gulp.task('build:post2', function() { 
    
return gulp.src('publish/js/esri/**/*',{ read: false })
    .pipe(gulpIgnore('**/images/**'))
    .pipe(gulpif(condition, gulp.dest('deploy/js/esri/')));
    //.pipe(rimraf({ force: true }));
});

gulp.task('build:post3', function() { 
return gulp.src('publish/js/dojo/**/*',{ read: false })
    .pipe(gulpIgnore.exclude('**/images/**'))
    .pipe(gulpIgnore.exclude('dojo.js'))
    .pipe(gulpIgnore.exclude('nls/dojo_en-us.js'))
    .pipe(rimraf());
});

gulp.task('build:post4', function() { 
return gulp.src(['publish/js/dojox/**/*'],{ read: false })
    .pipe(gulpIgnore.exclude('**/images/**'))
    .pipe(gulpIgnore.exclude('gfx/shape.js'))
    .pipe(gulpIgnore.exclude('gfx/path.js'))
    .pipe(gulpIgnore.exclude('gfx/svg.js'))
    .pipe(gulpIgnore.exclude('gfx/vml.js'))
    .pipe(gulpIgnore.exclude('gfx/arc.js'))
    .pipe(gulpIgnore.exclude('gfx/gradient.js'))
    .pipe(gulpIgnore.exclude('gfx/_base.js'))
    .pipe(rimraf())
});

gulp.task('build:post4', function() { 
return gulp.src(['publish/js/dijit/**/*'],{ read: false })
    .pipe(gulpIgnore.exclude('**/images/**'))
    .pipe(rimraf())
    
});
